library(methods)
setClass("ExportPopulation",
    slots = list(id = "character", father = "character", mother = "character", genotypes = "matrix", haplotypes = "matrix"),
    prototype = list(
        haplotypes = matrix() 
    )
)

#' Combine populations
#'
#' Combines multiple ExportPopulations together
#'
#' @param ... ExportPopulations to combine
#'
#' @return An ExportPopulation
#'
#' @export
#' 
c.ExportPopulation = function(...) {
    pops = list(...)

    combine_slots = function(input, slot_name, func){
        values = lapply(input, function(obj, slot_name){return(slot(obj, slot_name))}, slot_name = slot_name)
        output = do.call(func, values)
        return(output)
    }

    id = combine_slots(pops, "id", c)
    father = combine_slots(pops, "father", c)
    mother = combine_slots(pops, "mother", c)
    genotypes = combine_slots(pops, "genotypes", rbind)
    haplotypes = combine_slots(pops, "haplotypes", rbind)

    pop = new("ExportPopulation", id = id, father = father, mother = mother, genotypes = genotypes, haplotypes = haplotypes)
    return(pop)
}

#' Population length
#'
#' Get the length of an export population
#'
#' @param pop An ExportPopulation
#'
#' @return The length of the export population
#'
#' @export
#' 
length.ExportPopulation = function(pop) {
    return(length(pop@id))
}

#' Extract Subpopulations
#'
#' Extracts elements from an ExportPopulation
#'
#' @return An ExportPopulation
#'
#' @export
#' 
`[.ExportPopulation` <- function(x,i=NULL) {
    if(class(i) == "character"){
        locations = which(x@id %in% i)
    }else if(class(i) == "logical"){
        stopifnot(length(i) == length(x@id))
        locations = which(i)
    }
    else{
        locations = i
    }


    id = x@id[locations]
    father = x@father[locations]
    mother = x@mother[locations]
    genotypes = x@genotypes[locations,]

    haplotypes = matrix()
    if(nrow(x@haplotypes) == 2*length(x@id)) {
        hap_locations = do.call("c", lapply(locations, function(x) return(c(2*x-1, 2*x))))
        haplotypes = x@haplotypes[hap_locations,]
    }

    pop = new("ExportPopulation", id = id, father = father, mother = mother, genotypes = genotypes, haplotypes = haplotypes)
    return(pop)
}

#' Get fathers
#'
#' Returns the individuals in the population who are fathers.
#'
#' @param asr_pop AlphaSimR population
#'
#' @return An ExportPopulation
#'
#' @export
getFathers = function(pop){
    return(pop[pop@id %in% pop@father])
}

#' Get mothers
#'
#' Returns the individuals in the population who are mothers.
#'
#' @param asr_pop AlphaSimR population
#'
#' @return An ExportPopulation
#'
#' @export
getMothers = function(pop){
    return(pop[pop@id %in% pop@mother])
}

#' Get parents
#'
#' Returns the individuals in the population who are parents.
#'
#' @param asr_pop AlphaSimR population
#'
#' @return An ExportPopulation
#'
#' @export
getParents = function(pop){
    return(pop[pop@id %in% pop@father | pop@id %in% pop@mother])
}

#' Difference between populations.
#'
#' Returns the individuals in the population who are parents.
#'
#' @param asr_pop AlphaSimR population
#'
#' @return An ExportPopulation
#'
#' @export
diff.ExportPopulation = function(pop, popToRemove){
    return(pop[!(pop@id %in% popToRemove@id)])
}


#' Export population
#'
#' Converts and AlphaSimR Pop class to an 'ExportPopulation' class.
#'
#' @param asr_pop AlphaSimR population to convert
#' @param haplotypes A TRUE/FALSE value which indicates whether the haplotypes should be extracted.
#' @param qtl A TRUE/FALSE flag which indicates whether the QTL (TRUE) or SNP (FALSE) genotypes should be extracted.
#'
#' @return An ExportPopulation
#'
#' @export
#' 
exportPopulation = function(asr_pop, haplotypes = FALSE, qtl = FALSE) {
    pop_haplotypes = matrix()

    if(qtl) {
        pop_genotypes = AlphaSimR::pullQtlGeno(asr_pop)
    }
    else{
        pop_genotypes = AlphaSimR::pullSnpGeno(asr_pop)
    }

    if(haplotypes) {
        if(qtl) {
            pop_haplotypes = AlphaSimR::pullQtlHaplo(asr_pop)
        }
        else{
            pop_haplotypes = AlphaSimR::pullSnpHaplo(asr_pop)
        }
    }

    pop = new("ExportPopulation", id = asr_pop@id, father = asr_pop@father, mother = asr_pop@mother, genotypes = pop_genotypes, haplotypes = pop_haplotypes)
    return(pop)
}


#' Add Genotyping Errors
#'
#' Adds genotyping errors to an Export Population
#'
#' @param pop An Export Population
#' @param rate Genotyping error rate.
#'
#' @return An ExportPopulation with genotyping errors introduced
#'
#' @export
#' 
addGenotypingErrors = function(pop, rate) {
    genotypes = pop@genotypes

    markers = which(rbinom(length(genotypes), 1, rate) == 1)

    old_genotypes = genotypes[markers]
    new_genotypes = (old_genotypes + round(runif(length(old_genotypes), min = 1, max = 2))) %% 3 # Adds either a 1 or 2 offset and forces 0/1/2 values
    new_genotypes[old_genotypes == 9] = 9 # Forces missing genotypes to stay missing.

    genotypes[markers] = new_genotypes
    pop@genotypes = genotypes

    return(pop)
}

getMafMarkers = function(maf, nLD) {
    nSnp = length(maf)
    breaks = floor((0:(nLD))/(nLD) * nSnp)
    subLoci = rep(0, nLD)

    maf = 0.5 - abs(1-maf)/2
    maf = maf*(1-0.0001) + 0.0001

    for( i in 1:nLD) {
        start = breaks[i] + 1
        stop = breaks[i+1]

        regionalMaf = maf[start:stop]
        H = regionalMaf*log2(regionalMaf) + (1-regionalMaf)*log2(1-regionalMaf)
        H = -H #The paper doesn't say to do this, but this is what the examples work out to be.

        regionSize = stop - start + 1
        weights = 1-abs(1:regionSize - regionSize/2)/regionSize

        Hadj = H*weights

        subLoci[i] = start -1 + which.max(Hadj)[1]
    }

    return(subLoci)
}
getWeightedMarkers = function(maf, nLD) {
    nSnp = length(maf)
    breaks = floor((0:(nLD))/(nLD) * nSnp)
    subLoci = rep(0, nLD)

    maf = 0.5 - abs(1-maf)/2
    maf = maf*(1-0.0001) + 0.0001

    for( i in 1:nLD) {
        start = breaks[i] + 1
        stop = breaks[i+1]
        regionalMaf = maf[start:stop]

        subLoci[i] = start -1 + which.max(regionalMaf)[1]
    }

    return(subLoci)
}

#' Mask a genotype array
#'
#' Mask markers to create a low-density array. There are three options to use.
#'
#' @param pop An Export Population
#' @param nLD Number of low-density markers.
#' @param method A method to use to create the low-density array. Options: "even", "maf", "weighted"
#' @param maf A vector of minor-allele frequencies. Will be calculated from the genotypes if not supplied.
#' 
#' @details There are three options for the method which determine how the markers are chosen. 
#' In all methods, the markers are split `nLD` bins and a single marker is selected from each bin.
#' * `even` The middle marker in each bin is selected.
#' * `maf` The marker with the highest minor allele frequency is selected
#' * `weighted` the marker with both the highest minor allele frequency and centrality is selected. The weight function is given by:
#' \deqn{(1-d) (p log_2(p) + (1-p)log_2(1-p))}
#' where p is the minor allele frequency, 1-d is the distance (in percent) to the center of the bin.
#' @return An ExportPopulation with genotypes masked
#' @export
#' 


maskArray = function(pop, nLD, method = "even", maf = NULL) {
    genotypes = pop@genotypes
    
    if(is.null(maf)) {
        maf = colMeans(genotypes)/2
    }

    if(method == "even") {
        markers = floor((1:nLD)/(nLD + 1) * length(maf)) 

    }
    if(method == "maf") {
        markers = getMafMarkers(maf, nLD)
    }
    if(method == "weighted") {
        markers = getWeightedMarkers(maf, nLD)
    }

    genotypes[,-markers] = 9
    pop@genotypes = genotypes

    return(pop)
}

#' Add Missing Genotypes
#'
#' Adds spontaneous missing genotypes to an Export Population
#'
#' @param pop An Export Population
#' @param rate Genotype missing rate
#'
#' @return An ExportPopulation with missing genotypes introduced
#'
#' @export
#' 
maskSpontaneous = function(pop, rate){
    genotypes = pop@genotypes
    markers = which(rbinom(length(genotypes), 1, rate) == 1)
    genotypes[markers] = 9
    pop@genotypes = genotypes
    return(pop)
}

#' Write out an export population
#'
#' Adds spontaneous missing genotypes to an Export Population
#'
#' @param pop An Export Population
#' @param prefix A filename prefix. Files will be of the form, e.g., "prefix_pedigree.txt"
#' @param pedigree Should the pedigree file be written out?
#' @param genotypes Should the genotypes file be written out?
#' @param haplotypes Should the haplotypes file be written out?
#'
#' @return An ExportPopulation with missing genotypes introduced
#'
#' @export
#' 
write.pop = function(pop, prefix= NULL, pedigree = FALSE, genotypes = FALSE, haplotypes = FALSE) {
    # add an underscore if using a prefix.
    if(!is.null(prefix)) {
        prefix = paste0(prefix, "_")
    }

    if(class(pop) == "Pop"){
        pop = exportPopulation(pop, haplotypes = haplotypes) #Convert class over using SNP array.
    }
    if(pedigree) {
        pedigree = data.frame(id = pop@id, father = pop@father, mother = pop@mother, stringsAsFactors = FALSE)
        write.table(pedigree, paste0(prefix, "pedigree.txt"), col.names = F, row.names = F, quote = F)
    }
    if(genotypes) {
        write.table(cbind(pop@id, pop@genotypes), paste0(prefix, "genotypes.txt"), col.names = F, row.names = F, quote = F)
    }
    if(haplotypes) {
        haplotypes = pop@haplotypes

        indexes = lapply(1:nrow(pop@genotypes), function(index) {
            val0 = (index -1)*2 + 1
            val1 = (index -1)*2 + 2
            return(c(val1, val0))
        })
        indexes = do.call("c", indexes)
        haplotypes = haplotypes[indexes,]

        write.table(cbind(rep(pop@id, each = 2), haplotypes), paste0(prefix, "haplotypes.txt"), col.names = F, row.names = F, quote = F)
    }
}

