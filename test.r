library(AlphaSimR)
library(ASRExport)
nHD = 10
nSnp = 25
founderPop = runMacs(nInd = 200, nChr = 20, segSites = nSnp, inbred=FALSE)

# ---- Set Simulation Parameters ----

SP = SimParam$new(founderPop)
SP$addTraitA(nQtlPerChr=nSnp, mean=0, var=1)
SP$addSnpChip(nSnpPerChr=nSnp)
SP$setVarE(h2=.4)
SP$setGender("yes_sys")

gen0 = newPop(founderPop)

gen_parents = selectCross(gen0, nMale = 100, nFemale = 100, nCrosses = 500, nProgeny = 1, balance = TRUE)


export_gen = exportPopulation(gen_parents)

n_ind = length(export_gen)

# HD are all but 100 individuals.
hd = export_gen[sample(1:n_ind, n_ind - 100)]
ld = diff(export_gen, hd)

maf = rowMeans(export_gen@genotypes)
ld = maskArray(ld, 50, method = "weighted", maf = maf) # Mask all but 50 markers using the "weighted" method. Use external maf to reduce sampling bias from small population size.

combined = c(hd, ld)

combined = maskSpontaneous(combined, 0.05) # Add a 5% missing rate
combined = addGenotypingErrors(combined, 0.01) # Add a 1% genotyping error rate

write.pop(combined, prefix = NULL, pedigree = FALSE, genotypes = TRUE)


